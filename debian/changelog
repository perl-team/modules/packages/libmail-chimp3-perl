libmail-chimp3-perl (0.08-2) unstable; urgency=low

  [ Debian Janitor ]
  * Update standards version to 4.5.0, no changes needed.
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libstrictures-perl.
    + libmail-chimp3-perl: Drop versioned constraint on libstrictures-perl in
      Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 28 Aug 2022 14:42:55 +0100

libmail-chimp3-perl (0.08-1) unstable; urgency=medium

  * Team upload.

  * New upstream version 0.08
  * d/control:
    - Declare compliance with Debian Policy 4.4.1
    - Add Rules-Requires-Root field
    - Annotate test-only build dependencies with <!nocheck>
  * d/u/metadata:
    - Add Bug-Submit field; drop deprecated Contact, Name fields
  * d/watch:
    - Migrate to version 4 watch file format

 -- Nick Morrott <nickm@debian.org>  Tue, 26 Nov 2019 11:45:12 +0000

libmail-chimp3-perl (0.07-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.07
  * Bump debhelper-compat to 12
  * Bump Standards-Version to 4.4.0
  * Update d/control w.r.t cme

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Sat, 27 Jul 2019 06:56:16 +0530

libmail-chimp3-perl (0.06-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.06.
    Fixes "FTBFS: test failures with new Web::API"
    (Closes: #918380)
  * Declare compliance with Debian Policy 4.3.0.

 -- gregor herrmann <gregoa@debian.org>  Mon, 28 Jan 2019 17:53:03 +0100

libmail-chimp3-perl (0.04-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update GitHub URLs to use HTTPS.

  [ Christopher Hoskin ]
  * Bump Standards-Version from 4.1.3 to 4.2.1 (no change required)

 -- Christopher Hoskin <mans0954@debian.org>  Fri, 26 Oct 2018 20:55:20 +0100

libmail-chimp3-perl (0.04-1) unstable; urgency=low

  * Initial release. (Closes: #888643: ITP: libmail-chimp3-perl --
    interface to mailchimp.com's RESTful Web API v3)

 -- Christopher Hoskin <mans0954@debian.org>  Sun, 28 Jan 2018 07:19:51 +0000
